using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.SceneTemplate;
using UnityEngine;

public class score : MonoBehaviour
{

    private float time = 0;

    private int tiempoDificultad = 10;

    /*
    public GameObject gunter;
    public GameObject golem;
    public GameObject elefante;
    */
    //public PrefabAssetType;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //TMPro.TextMeshProUGUI componentText = this.GetComponent<TextMeshProUGUI>();


        //Time.deltaTime el temps que ha passat entre l'ultim frame i l'actual
        time += Time.deltaTime;

        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Score: " + (int) time;

        spawnerEnemies.extraSpd = (int) time / tiempoDificultad;
        Parallax.extraSpd = (int)time / tiempoDificultad;
        backObj.extraSpd = (int)time / tiempoDificultad;
        if (spawnerEnemies.cooldown > 1)
        {
            spawnerEnemies.cooldown = spawnerEnemies.cooldown - (int)(((int)time / 40) - 0.7);
        }

        //print(time % 5);
        /*
        if ((int) time % 5 == 0)
        {
            if ((int) time != spawnerEnemies.extraSpd)
            {
                spawnerEnemies.extraSpd += 1;
            }
        }
        */

    }
}
