using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startpos;
    public int spd = 3;
    private bool back2 = false;

    public static int extraSpd = 0;
    /*
    public float parallaxEffect;
    */

    // Start is called before the first frame update
    void Start()
    {
        
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        print(length);
        
        }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd-extraSpd, 0);

        if ((transform.position.x < -1) && !back2)
        {
            GameObject background = Instantiate(this.gameObject);
            background.transform.position = new Vector3(transform.position.x + length, transform.position.y, transform.position.z);
            if (background.GetComponent<SpriteRenderer>().flipX) background.GetComponent<SpriteRenderer>().flipX = false;
            else if (!background.GetComponent<SpriteRenderer>().flipX) background.GetComponent<SpriteRenderer>().flipX = true;
            back2 = true;
        }

        if ((transform.position.x + length / 2) < -12)
        {
            Destroy(this.gameObject);
        }
        /*
        if (transform.position.x > startpos + length)
        {
            startpos += length;
            transform.position = new Vector3(startpos + length, transform.position.y, transform.position.z);
        } else if (transform.position.x < startpos - length)
        {
            startpos -= length;
            transform.position = new Vector3(startpos - length, transform.position.y, transform.position.z);
        }
        */
        /*
        float temp = (cam.transform.position.x * (1 - parallaxEffect));
        float dist = (cam.transform.position.x * parallaxEffect);

        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if (temp > startpos + length)
        {
            startpos += length;
            print(startpos);
        }
        else if (temp < startpos - length)
        {
            startpos -= length;
            print(startpos);
        }
        */
    }
}
