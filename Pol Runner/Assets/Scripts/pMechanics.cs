using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pMechanics : MonoBehaviour
{

    Rigidbody2D player;

    private int spd = 6;

    private int jump = 500;

    private bool jumping = false;

    private bool stopJumping = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)) {
            player.velocity = new Vector2(-spd, player.velocity.y);
        } else if (Input.GetKey(KeyCode.D)) {
            player.velocity = new Vector2(spd, player.velocity.y);    
        } else {
            player.velocity = new Vector2(0, player.velocity.y);  
        }


        if (Input.GetKeyDown(KeyCode.Space) && !jumping) {
            player.AddForce(new Vector2(0, jump));
            //print("SALTO [ jumping: " + jumping + ", stopJumping: " + stopJumping + "]");
            jumping = true;
            stopJumping = false;
            StartCoroutine(saltando());
            //print("Coroutine ha comenzado [ jumping: " + jumping + ", stopJumping: " + stopJumping + "]");
        } else if (Input.GetKey(KeyCode.Space) && !stopJumping) {
            player.AddForce(new Vector2(0, 1.5f));
            //print("SIUUUUUUUUU [ jumping: " + jumping + ", stopJumping: " + stopJumping + "]");
        } else if (jumping) {
            stopJumping = true;
        }

    }

    private IEnumerator saltando()
    {
        yield return new WaitForSeconds(0.5f);
        stopJumping = true;
        //print("Coroutine ha finalizado [ jumping: " + jumping + ", stopJumping: " + stopJumping + "]");
    }

    public void IgnoreLayerCollision(int layer1, int layer2, bool ignore = true)
    {

    }


    public void OnCollisionEnter2D(Collision2D collision) {
            
            if (collision.transform.tag == "object")
            {
                this.jumping = false;
            }

            //IgnoreLayerCollision(8, 10, true);

            if (collision.transform.tag == "enemy")
            {
                Destroy(this.gameObject);
                print("Game Over");

                SceneManager.LoadScene("GameOverR");
            }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        
    }
}
