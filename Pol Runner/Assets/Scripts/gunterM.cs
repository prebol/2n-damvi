using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunterM : MonoBehaviour
{
    public int spd = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, this.GetComponent<Rigidbody2D>().velocity.y);

        if (transform.position.x < -15)
        {
            Destroy(this.gameObject);
        }
    }

    /*
    public void OnTriggerEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "object")
        {
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
        }
    }
    */
}
